from ..connection import async_session
from sqlalchemy import delete
from ..models import Followers
from typing import Dict


async def add_following(data: Dict) -> None:
    async with async_session() as session:
        async with session.begin():
            follower = Followers(**data)
            session.add(follower)


async def delete_following(user_id: int, following_id: int) -> None:
    async with async_session() as session:
        async with session.begin():
            await session.execute(
                delete(Followers).where(
                    following_id == Followers.user_id,
                    user_id == Followers.follower_id
                )
            )
