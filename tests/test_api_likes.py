import pytest
from fastapi_app.database.models import Likes
from sqlalchemy import select, exists


def test_response(client):
    response = client.post(
        "/api/tweets/1/likes", headers={"api-key": "test"}
    )
    like_data = response.json()

    assert response.status_code == 200
    assert like_data.get('result')


@pytest.mark.asyncio
async def test_like_added(db):
    result = await db.execute(
        select(
            exists().where(
                Likes.tweet_id == 1,
                Likes.user_id == 1
            )
        )
    )
    is_likes = result.scalar()

    assert is_likes


def test_method_delete(client):
    response = client.delete(
        "/api/tweets/1/likes", headers={"api-key": "test"}
    )
    like_data = response.json()

    assert response.status_code == 200
    assert like_data.get('result')
