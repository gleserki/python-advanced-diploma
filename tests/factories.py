import factory
from fastapi_app.database.models import Tweets, User
import factory.fuzzy as fuzzy


class TweetsFactory(factory.Factory):
    class Meta:
        model = Tweets

    user_id = 1
    content = fuzzy.FuzzyText(
        length=20,
        chars="qwertyuiopasdfghjklzxcvbnm"
    )


class UserFactory(factory.Factory):
    class Meta:
        model = User

    name = factory.Faker('first_name')
    api_key = fuzzy.FuzzyText(length=10)
