"""insert_users

Revision ID: 88d52a06130b
Revises: e7738bd4870f
Create Date: 2024-03-27 18:43:06.951047

"""
from typing import Sequence, Union

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision: str = '88d52a06130b'
down_revision: Union[str, None] = 'e7738bd4870f'
branch_labels: Union[str, Sequence[str], None] = None
depends_on: Union[str, Sequence[str], None] = None


def upgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.execute("INSERT INTO users (name, api_key) VALUES ('gleb', 'test')")
    # ### end Alembic commands ###


def downgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.execute("DELETE FROM users WHERE api_key = 'test'")
    # ### end Alembic commands ###
