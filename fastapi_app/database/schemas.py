from pydantic import BaseModel, Field
from typing import List


class BaseResponse(BaseModel):
    result: bool


class Follower(BaseModel):
    id: int
    name: str


class Following(BaseModel):
    id: int
    name: str


class User(BaseModel):
    id: int
    name: str
    followers: List[Follower]
    following: List[Following]


class UserOutput(BaseResponse):
    user: User


class Author(BaseModel):
    id: int
    name: str


class Likes(BaseModel):
    user_id: int = Field(..., alias="id")
    name: str


class Tweets(BaseModel):
    id: int
    content: str
    attachments: List[str]
    author: Author
    likes: List[Likes]


class TweetsOutput(BaseResponse):
    tweets: List[Tweets]


class TweetBody(BaseModel):
    content: str = Field(..., alias="tweet_data")
    tweet_media_ids: List[int]


class TweetPostResponse(BaseResponse):
    tweet_id: int


class MediaPostResponse(BaseResponse):
    media_id: int


class ErrorResponse(BaseResponse):
    error_type: str
    error_message: str
