from typing import Dict, List
from ..connection import async_session
from ..models import Medias
from sqlalchemy import select


async def add_media(data: Dict) -> int:
    """

    :param data: Dict
    :return: media_id - int
    """
    async with async_session() as session:
        async with session.begin():
            media = Medias(**data)
            session.add(media)
    return media.id


async def get_media_rel_paths(media_ids: List[int]) -> List[str]:
    async with async_session() as session:
        async with session.begin():
            res = await session.execute(
                select(Medias.relative_path).where(
                    Medias.id.in_(media_ids)
                )
            )
        return res.scalars().all()
