from ..connection import async_session
from aiofiles import os
from sqlalchemy import select, delete
from ..models import Tweets, Medias, Likes
from typing import Dict


async def add_tweet(data: Dict) -> int:
    """

    :param data: Dict
    :return: tweet_id - int
    """
    async with async_session() as session:
        async with session.begin():
            tweet = Tweets(**data)
            session.add(tweet)
    return tweet.id


async def delete_tweet(tweet_id: int) -> None:
    async with async_session() as session:
        async with session.begin():
            res = await session.execute(
                select(Tweets).where(tweet_id == Tweets.id)
            )
            tweet = res.scalar()

            for attachment in tweet.attachments:
                await os.remove(attachment)

            await session.execute(
                delete(Medias).where(
                    Medias.id.in_(tweet.tweet_media_ids)
                )
            )
            await session.execute(
                delete(Likes).where(tweet_id == Likes.tweet_id)
            )
            await session.execute(
                delete(Tweets).where(tweet_id == Tweets.id)
            )
