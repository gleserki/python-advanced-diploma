from sqlalchemy import select, exists
from fastapi_app.database.models import Likes
import pytest


def test_response_method_get(client):
    response = client.get("/api/tweets", headers={"api-key": "test"})
    tweets_data = response.json()
    tweets = tweets_data.get('tweets')

    assert response.status_code == 200
    assert len(tweets) == 3


def test_response_method_post(client):
    response = client.post(
        "/api/tweets",
        headers={"api-key": "test"},
        json={"tweet_data": "hello", "tweet_media_ids": []}
    )
    tweet_data = response.json()
    tweet_id = tweet_data.get('tweet_id')

    assert response.status_code == 200
    assert tweet_id == 4


def test_method_delete(client):
    response = client.delete(
        "/api/tweets/3",
        headers={"api-key": "test"}
    )
    data = response.json()

    assert response.status_code == 200
    assert data.get("result")


@pytest.mark.asyncio
async def test_likes_deletion_after_tweet_delete(db):

    result = await db.execute(
        select(exists().where(Likes.tweet_id == 3))
    )
    is_likes = result.scalar()

    assert not is_likes
