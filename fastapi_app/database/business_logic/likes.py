from typing import Dict
from ..connection import async_session
from ..models import Likes
from sqlalchemy import delete


async def add_like(data: Dict) -> None:
    async with async_session() as session:
        async with session.begin():
            like = Likes(**data)
            session.add(like)


async def delete_like(tweet_id: int, user_id: int) -> None:
    async with async_session() as session:
        async with session.begin():
            await session.execute(
                delete(Likes).where(
                    tweet_id == Likes.tweet_id,
                    user_id == Likes.user_id
                )
            )
