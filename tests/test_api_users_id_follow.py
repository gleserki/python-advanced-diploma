
def test_response_method_post(client):
    response = client.post(
        "/api/users/2/follow",
        headers={"api-key": "test"},
    )
    data = response.json()

    assert response.status_code == 200
    assert data['result']


def test_response_method_delete(client):
    response = client.delete(
        "/api/users/2/follow",
        headers={"api-key": "test"},
    )
    data = response.json()

    assert response.status_code == 200
    assert data['result']
