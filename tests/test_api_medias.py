import os
import pytest
from sqlalchemy import select
from fastapi_app.database.models import Medias
from aiofiles.os import remove


def test_response(client):
    image_dir = os.path.join(os.path.dirname(__file__), "images")

    with open(f'{image_dir}/test_image.png', "rb") as file:
        response = client.post(
            "/api/medias",
            files={"file": ("test_image.png", file, "image/png")},
            headers={"api-key": "test"}
        )

    media_data = response.json()

    assert response.status_code == 200
    assert media_data.get('result')


@pytest.mark.asyncio
async def test_file_existing(db):
    res = await db.execute(select(Medias).where(Medias.id == 1))
    media: Medias = res.scalar()
    filename = media.filename
    ext = media.extension

    app_directory = os.path.join(os.path.abspath("."), "fastapi_app")
    relpath = os.path.relpath(app_directory, os.getcwd())
    if os.path.exists(f"{relpath}/static/media/{filename}.{ext}"):
        await remove(f"{relpath}/static/media/{filename}.{ext}")
        assert True
    else:
        assert False
