import pytest_asyncio
from fastapi_app.database.connection import (
    async_session, engine, Base
)
from fastapi_app.database.models import User, Likes
from fastapi_app.application import app
from fastapi.testclient import TestClient
from .factories import TweetsFactory, UserFactory
import pytest
import asyncio


@pytest.fixture(scope="session")
def event_loop():
    policy = asyncio.get_event_loop_policy()
    loop = policy.new_event_loop()
    yield loop
    loop.close()


@pytest_asyncio.fixture(scope="session")
async def db_engine():
    async with engine.begin() as conn:
        await conn.run_sync(Base.metadata.create_all)
    yield engine
    async with engine.begin() as conn:
        await conn.run_sync(Base.metadata.drop_all)
    await engine.dispose()


@pytest_asyncio.fixture(scope="session")
async def init_data(db_engine):
    """
    this fixture have some dependencies like engine
    event_loop and some init data

    use it for init it all in your client fixture
    """
    async with async_session() as session:
        async with session.begin():
            session.add_all(
                [
                    User(name="Gleb", api_key="test"),
                    UserFactory(), UserFactory(),
                    TweetsFactory(), TweetsFactory(), TweetsFactory()
                ]
            )
        async with session.begin():
            session.add(Likes(tweet_id=3, user_id=1))


@pytest.fixture(scope="session")
def client(init_data):
    with TestClient(app) as test_client:
        yield test_client


@pytest_asyncio.fixture()
async def db():
    async with async_session() as session:
        yield session
