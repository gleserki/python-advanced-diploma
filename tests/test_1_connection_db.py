import pytest
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy import text


@pytest.mark.asyncio
async def test_connection(db: AsyncSession):
    """
    Test connection to db

    This test create table, insert data and check
    if data insert correctly, then drop table
    """
    async with db.begin():
        await db.execute(
            text("create table test_1 (id integer primary key)")
        )
        await db.execute(
            text("insert into test_1 (id) values (1)")
        )

        result = await db.execute(
            text("select * from test_1 where id = 1")
        )
        await db.execute(
            text("drop table test_1")
        )

    assert result.scalar() == 1
