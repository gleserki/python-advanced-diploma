import pytest
from fastapi.exceptions import ValidationException


@pytest.mark.parametrize(
    "url, api_keys, method",
    [
        ("/api/users/me", ["123", "123.65", "None"], "GET")
    ]
)
def test_api_key_validation(
        client, url: str, api_keys: list[str], method: str
):
    for api_key in api_keys:
        with pytest.raises(ValidationException):
            client.request(
                url=url,
                headers={"api-key": api_key},
                method=method
            )

