from typing import List

from ..connection import async_session
from sqlalchemy import select
from ..models import User, Tweets
from sqlalchemy.orm import selectinload, joinedload


async def get_user_by_api_key(api_key: str) -> User:
    async with async_session() as session:
        result = await session.execute(
            select(User).options(
                selectinload(User.followers),
                selectinload(User.following),
                selectinload(User.tweets).options(
                    joinedload(Tweets.author),
                    selectinload(Tweets.likes)
                )
            ).where(User.api_key == api_key)
        )
        return result.scalar()


async def get_user_by_name(username: str) -> User:
    async with async_session() as session:
        result = await session.execute(
            select(User).options(
                selectinload(User.followers),
                selectinload(User.following),
            ).where(User.name == username)
        )
        return result.scalar()


async def get_user_tweets_by_id(user_id: int) -> List[Tweets]:
    async with async_session() as session:
        result = await session.execute(
            select(User).options(
                selectinload(User.tweets).options(
                    joinedload(Tweets.author),
                    selectinload(Tweets.likes)
                )
            ).where(User.id == user_id)
        )
        return result.scalar().tweets
