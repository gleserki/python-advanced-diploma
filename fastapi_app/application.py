from fastapi import FastAPI, Request
from fastapi.responses import JSONResponse

from database.connection import engine, Base
from database.schemas import ErrorResponse
from routers import (
    users, tweets, medias
)

app = FastAPI()
app.include_router(users.router)
app.include_router(tweets.router)
app.include_router(medias.router)


@app.on_event("shutdown")
async def shutdown():
    await engine.dispose()


@app.exception_handler(Exception)
async def http_exception_handler(_: Request, exc: Exception):
    error = ErrorResponse(
        result=False,
        error_type=exc.__class__.__name__,
        error_message=exc.__str__()
    )
    return JSONResponse(content=error.model_dump(), status_code=500)
