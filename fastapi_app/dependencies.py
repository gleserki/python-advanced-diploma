from fastapi import Header
from database.business_logic import users


async def get_current_user(
        api_key: str = Header(
            None, description="API key for user auth"
        )
):
    user = await users.get_user_by_api_key(api_key)
    return user
