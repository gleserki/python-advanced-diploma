from fastapi import (
    APIRouter, Depends, Path
)
from database.schemas import (
    ErrorResponse, TweetsOutput, BaseResponse,
    TweetPostResponse, TweetBody
)
from database.models import User
from database.business_logic import (
    medias, tweets, likes
)

from dependencies import get_current_user


router = APIRouter(
    prefix="/api/tweets",
    responses={500: {"model": ErrorResponse}},
)


@router.get(
    '/', response_model=TweetsOutput,
)
async def get_tweets(
        user: User = Depends(get_current_user)
):
    return {"result": True, "tweets": user.tweets}


@router.post(
    '/', response_model=TweetPostResponse,
)
async def add_tweet(
        body: TweetBody,
        user: User = Depends(get_current_user)
):
    body = body.model_dump()
    body["user_id"] = user.id
    body['attachments'] = \
        await medias.get_media_rel_paths(body["tweet_media_ids"])
    tweet_id = await tweets.add_tweet(body)
    return {"result": True, "tweet_id": tweet_id}


@router.delete(
    '/{id}', response_model=BaseResponse,
)
async def delete_tweet(
        tweet_id: int = Path(..., alias="id"),
        _: User = Depends(get_current_user)
):
    await tweets.delete_tweet(tweet_id)
    return {"result": True}


@router.post(
    '/{id}/likes', response_model=BaseResponse,
)
async def add_like(
        tweet_id: int = Path(..., alias="id"),
        user: User = Depends(get_current_user)
):
    data = {
        "user_id": user.id,
        "tweet_id": tweet_id
    }
    await likes.add_like(data)
    return {"result": True}


@router.delete(
    '/{id}/likes', response_model=BaseResponse,
)
async def delete_like(
        tweet_id: int = Path(..., alias="id"),
        user: User = Depends(get_current_user)
):
    await likes.delete_like(tweet_id, user.id)
    return {"result": True}
