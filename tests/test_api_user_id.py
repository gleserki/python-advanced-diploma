
def test_response(client):
    response = client.get("/api/users/1")
    user_data = response.json()
    user_id = user_data.get('user').get('id')

    assert response.status_code == 200
    assert user_id == 1
