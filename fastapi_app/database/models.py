from .connection import Base
from sqlalchemy import (
    Column, String, Integer,
    ForeignKey, Text, ARRAY

)
from sqlalchemy.orm import relationship


class User(Base):
    __tablename__ = 'users'

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String(20), index=True, nullable=False)

    followers = relationship(
        "User",
        secondary="followers",
        primaryjoin="User.id == Followers.user_id",
        secondaryjoin="User.id == Followers.follower_id"
    )
    following = relationship(
        "User",
        secondary="followers",
        primaryjoin="User.id == Followers.follower_id",
        secondaryjoin="User.id == Followers.user_id",
        overlaps="followers"
    )

    tweets = relationship("Tweets", back_populates="author")

    api_key = Column(String(20), index=True, nullable=False)


class Tweets(Base):
    __tablename__ = 'tweets'

    id = Column(Integer, primary_key=True, index=True)
    content = Column(Text, index=True, nullable=False)
    user_id = Column(ForeignKey("users.id"), nullable=False)
    attachments = Column(ARRAY(String), default=[], index=True)
    tweet_media_ids = Column(ARRAY(Integer), default=[], index=True)

    author = relationship("User", back_populates="tweets")
    likes = relationship("User", secondary="likes")


class Followers(Base):
    __tablename__ = 'followers'

    user_id = Column(ForeignKey("users.id"), primary_key=True)
    follower_id = Column(ForeignKey("users.id"), primary_key=True)


class Likes(Base):
    __tablename__ = 'likes'

    tweet_id = Column(ForeignKey("tweets.id"), primary_key=True)
    user_id = Column(ForeignKey("users.id"), primary_key=True)


class Medias(Base):
    __tablename__ = 'medias'

    id = Column(Integer, primary_key=True, index=True)
    filename = Column(String(50), nullable=False, index=True)
    extension = Column(String(10), nullable=False, index=True)
    relative_path = Column(String(100), nullable=False, index=True)
