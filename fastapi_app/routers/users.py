from fastapi import (
    APIRouter, Depends, Path
)
from database.schemas import (
    ErrorResponse, UserOutput, BaseResponse,
    TweetsOutput
)
from database.models import User
from database.business_logic import users, followers

from dependencies import get_current_user

router = APIRouter(
    prefix="/api/users",
    responses={500: {"model": ErrorResponse}},
)


@router.get(
    '/me', response_model=UserOutput,
)
async def get_user_info(
        user: User = Depends(get_current_user)
):
    return {"result": True, "user": user}


@router.get(
    '/{username}', response_model=UserOutput,
)
async def get_user_info_by_name(
    username: str = Path(..., alias="username"),
):
    user = await users.get_user_by_name(username)
    return {"result": True, "user": user}


@router.post(
    '/{id}/follow', response_model=BaseResponse
)
async def add_following(
        user_id: int = Path(..., alias="id"),
        user: User = Depends(get_current_user)
):
    data = {
        "user_id": user_id,
        "follower_id": user.id
    }
    await followers.add_following(data)
    return {"result": True}


@router.delete(
    '/{id}/follow', response_model=BaseResponse
)
async def delete_following(
        following_id: int = Path(..., alias="id"),
        user: User = Depends(get_current_user)
):
    await followers.delete_following(user.id, following_id)
    return {"result": True}

@router.get(
    '/{id}/tweets', response_model=TweetsOutput,
)
async def get_user_tweets_by_id(
    user_id: int = Path(..., alias="id"),
):
    tweets = await users.get_user_tweets_by_id(user_id)
    return {"result": True, "tweets": tweets}
