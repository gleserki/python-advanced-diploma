import os
import uuid
import aiofiles
from fastapi import (
    APIRouter, Depends, UploadFile
)
from hashids import Hashids
from database.schemas import (
    ErrorResponse, MediaPostResponse
)
from database.models import User
from database.business_logic import medias

from dependencies import get_current_user


router = APIRouter(
    prefix="/api/medias",
    responses={500: {"model": ErrorResponse}},
)


@router.post(
    '/', response_model=MediaPostResponse,
)
async def add_media(
        file: UploadFile,
        _: User = Depends(get_current_user)
):
    salt = str(uuid.uuid4())
    hashids = Hashids(salt=salt, min_length=7)
    filename_uid = hashids.encode(12345678910)
    _, ext = file.content_type.split('/')

    app_directory = os.path.join(os.path.dirname(__file__), "..")
    relpath = os.path.relpath(app_directory, os.getcwd())

    data = {
        "filename": filename_uid,
        "extension": ext,
        "relative_path": f"{relpath}/static/media/{filename_uid}.{ext}"
    }

    media_id = await medias.add_media(data)

    async with aiofiles.open(
            f'{relpath}/static/media/{filename_uid}.{ext}', mode='wb'
    ) as f:
        await f.write(file.file.read())

    return {"result": True, "media_id": media_id}
